package cn.miaopasi.study.bootminio.client;

import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import cn.miaopasi.study.bootminio.config.MinioConfig;
import cn.miaopasi.study.bootminio.type.MimeTypeEnum;
import io.minio.GetObjectResponse;
import io.minio.messages.Bucket;
import io.minio.messages.Tags;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class MinioClientTemplateTest {

    @Resource
    private MinioClientTemplate minioClientTemplate;
    @Resource
    private MinioConfig minioConfig;

    @DisplayName("存储桶是否存在")
    @Test
    void bucketExists() {
        boolean bucketExists = this.minioClientTemplate.bucketExists("test");
        Assertions.assertTrue(bucketExists);
    }

    @DisplayName("创建存在的存储桶")
    @Test
    void makeBucket() {
        boolean makeBucket = this.minioClientTemplate.makeBucket("test");
        Assertions.assertTrue(makeBucket);
    }

    @DisplayName("创建不存在的储桶")
    @Test
    void makeBucket0001() {
        String bucketName = "user-10001";
        // 如果存在就删除
        boolean bucketExists = this.minioClientTemplate.bucketExists(bucketName);
        if (bucketExists) {
            this.minioClientTemplate.removeBucket(bucketName);
        }
        // 创建不存在的存储桶
        boolean makeBucket = this.minioClientTemplate.makeBucket(bucketName);
        Assertions.assertTrue(makeBucket);
    }

    @DisplayName("删除存储桶")
    @Test
    void removeBucket() {
        // 先创建一个测试的存储桶
        String bucketName = "user-10001";
        this.minioClientTemplate.makeBucket(bucketName);
        // 测试删除
        boolean bool = this.minioClientTemplate.removeBucket("user-10001");
        Assertions.assertTrue(bool);
    }

    @DisplayName("存储桶列表")
    @Test
    void buckets() {
        List<Bucket> buckets = this.minioClientTemplate.buckets();
        Assertions.assertNotNull(buckets);
        Assertions.assertFalse(buckets::isEmpty);
        Assertions.assertEquals(buckets.get(0).name(), "test");
    }

    @DisplayName("设置存储桶标签")
    @Test
    void setBucketTags() {
        Map<String, String> tags = new HashMap<>();
        tags.put("type", "0");
        tags.put("md5", "098f6bcd4621d373cade4e832627b4f6");
        boolean bool = this.minioClientTemplate.setBucketTags("test", tags);
        Assertions.assertTrue(bool);
    }

    @DisplayName("创建存储桶并设置标签")
    @Test
    void makeBucketAndSetTags() {
        Map<String, String> tags = new HashMap<>();
        tags.put("type", "0");
        tags.put("md5", "fa820cc1ad39a4e99283e9fa555035ec");
        boolean makeBucket = this.minioClientTemplate.makeBucket("test001", tags);
        Assertions.assertTrue(makeBucket);
    }

    @DisplayName("读取存储桶标签")
    @Test
    void getBucketTags() {
        Tags bucketTags = this.minioClientTemplate.getBucketTags("test001");
        Assertions.assertNotNull(bucketTags);
        Assertions.assertNotNull(bucketTags.get());
        Assertions.assertEquals(bucketTags.get().get("md5"), "fa820cc1ad39a4e99283e9fa555035ec");
    }

    @DisplayName("获取对象信息")
    @Test
    void getObject() {
        GetObjectResponse response = this.minioClientTemplate.getObject("test", "temp/banner1.png");
        Assertions.assertNotNull(response);
        Assertions.assertEquals(response.bucket(), "test");
        Assertions.assertEquals(response.object(), "temp/banner1.png");
    }

    @DisplayName("上传文件到指定存储桶")
    @Test
    void uploadToBucket() throws IOException {
        String bucketName = "test";
        // 先创建存储桶，防止存储桶不存在
        this.minioClientTemplate.makeBucket(bucketName);

        // 读取文件信息
        File tempFile = FileUtil.file("D:\\develop\\ps\\banner1.png");
        String fileName = FileUtil.getName(tempFile);
        // 获取文件的类型
        BufferedInputStream stream = IoUtil.toBuffered(IoUtil.toStream(tempFile));
        stream.mark(0);
        String fileType = FileTypeUtil.getType(stream, fileName);
        stream.reset();
        String contentType = MimeTypeEnum.getContentType(fileType);

        // 上传文件
        String path = StrUtil.format("temp/{}", fileName);
        String uploadPath = this.minioClientTemplate.upload(bucketName, path, stream, tempFile.length(), contentType);

        Assertions.assertNotNull(uploadPath);
        Assertions.assertEquals(uploadPath, "temp/banner1.png");
    }

    @DisplayName("上传文件到默认存储桶")
    @Test
    void uploadToDefaultBucket() throws IOException {
        // 读取文件信息
        File tempFile = FileUtil.file("D:\\develop\\ps\\活动创建指引.psd");
        String fileName = FileUtil.getName(tempFile);
        // 获取文件流
        BufferedInputStream stream = IoUtil.toBuffered(IoUtil.toStream(tempFile));
        // 上传文件
        String path = StrUtil.format("temp/{}", fileName);
        String bucket = minioConfig.getBucket();
        String uploadPath = this.minioClientTemplate.upload(bucket, path, stream, tempFile.length());

        Assertions.assertNotNull(uploadPath);
        Assertions.assertEquals(uploadPath, "temp/活动创建指引.psd");
    }

    @DisplayName("上传文件到指定存储桶")
    @Test
    void filePathUploadToBucket() throws IOException {
        String bucketName = "test";
        // 先创建存储桶，防止存储桶不存在
        this.minioClientTemplate.makeBucket(bucketName);

        // 读取文件信息
        String sourcePath = "D:\\develop\\ps\\APP.fig";
        String fileName = FileUtil.getName(sourcePath);

        // 上传文件
        String targetPath = StrUtil.format("temp/{}", fileName);
        String uploadPath = this.minioClientTemplate.upload(bucketName, targetPath, sourcePath);

        Assertions.assertNotNull(uploadPath);
        Assertions.assertEquals(uploadPath, "temp/APP.fig");

    }

    @DisplayName("下载文件到本地")
    @Test
    void download() {
        String filePath = "temp/banner1.png";
        String savePath = "D:\\demo\\banner1.png";
        this.minioClientTemplate.download("test", filePath, savePath);
    }

    @DisplayName("下载文件到流")
    @Test
    void downloadToOutputStream() {
        String filePath = "temp/APP.fig";
        BufferedOutputStream outputStream = FileUtil.getOutputStream("D:\\demo\\APP_new.fig");
        this.minioClientTemplate.download("test", filePath, outputStream);
        IoUtil.close(outputStream);
    }

    @DisplayName("删除文件")
    @Test
    void removeObject() {
        String bucketName = "test";
        String filePath = "temp/20221115100715.png";
        this.minioClientTemplate.removeObject(bucketName, filePath);

        // 校验文件是否被删除
        GetObjectResponse response = this.minioClientTemplate.getObject(bucketName, filePath);
        Assertions.assertNull(response);
    }
}