package cn.miaopasi.study.bootminio.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.ContentType;
import cn.hutool.json.JSONUtil;
import cn.miaopasi.study.bootminio.BootminioApplication;
import cn.miaopasi.study.bootminio.model.R;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = BootminioApplication.class)
@WebAppConfiguration
class MinioControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void upload() throws Exception {
        InputStream inputStream = FileUtil.getInputStream("D:\\develop\\ps\\ad\\ad1210.psd");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "ad1210.psd", MediaType.APPLICATION_OCTET_STREAM_VALUE, inputStream);
        String response = mockMvc.perform(MockMvcRequestBuilders.multipart("/fs/upload/file")
                        .file(multipartFile)
                        .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        R res = JSONUtil.toBean(response, R.class);
        Assertions.assertNotNull(res);
        Assertions.assertEquals(res.getCode(), 0);
    }

    @Test
    void uploadTime() throws Exception {
        InputStream inputStream = FileUtil.getInputStream("D:\\develop\\ps\\ad\\ad1210.psd");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "ad1210.psd", MediaType.APPLICATION_OCTET_STREAM_VALUE, inputStream);
        String response = mockMvc.perform(MockMvcRequestBuilders.multipart("/fs/upload/fileRname")
                        .file(multipartFile)
                        .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        R res = JSONUtil.toBean(response, R.class);
        Assertions.assertNotNull(res);
        Assertions.assertEquals(res.getCode(), 0);
    }

    @Test
    void delete() throws Exception {
        String response = mockMvc.perform(MockMvcRequestBuilders.post("/fs/delete")
                        .param("filePath", "/temp/banner1.png")
                        .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        R res = JSONUtil.toBean(response, R.class);
        Assertions.assertNotNull(res);
        Assertions.assertEquals(res.getCode(), 0);
        Assertions.assertEquals(res.getMsg(), "删除成功");
    }
}