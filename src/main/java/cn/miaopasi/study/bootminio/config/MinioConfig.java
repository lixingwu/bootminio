package cn.miaopasi.study.bootminio.config;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置Minio客户端.
 *
 * @author Administrator
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "minio")
public class MinioConfig {

    /**
     * 端点
     */
    private String endpoint;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 默认打开存储桶
     */
    private String bucket;

    /**
     * 默认访问的根路径
     */
    private String rootPath;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(this.endpoint)
                .credentials(this.username, this.password)
                .build();
    }

}
