package cn.miaopasi.study.bootminio.model;

import lombok.Data;

/**
 * 通用结果集
 *
 * @author lixingwu
 */
@Data
public class R<T> {
    private static final long serialVersionUID = 1L;

    private Integer code;
    private String msg;
    private T data;

    public R() {
    }

    public R(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static R<Object> ok() {
        return new R<>(0, "成功", null);
    }

    public static R<Object> ok(String msg) {
        return new R<>(0, msg, null);
    }

    public static <T> R<T> ok(String msg, T data) {
        return new R<>(0, msg, data);
    }

    public static R<Object> fail(Integer code) {
        return new R<>(code, "失败", null);
    }

    public static R<Object> fail(Integer code, String msg) {
        return new R<>(code, msg, null);
    }

}
